import React from 'react';
import InclusiveWorkplaceCalendar from "./views/inclusive-workplace-calendar/inclusive-workplace-calendar";

function App() {
    return (
        <InclusiveWorkplaceCalendar/>
    );
}

export default App;
