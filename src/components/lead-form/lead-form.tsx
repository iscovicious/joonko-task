import React, {useState} from 'react';
import './lead-form.scss'
import {api} from "../../service/api";

interface LeadFormProps {
}

interface LeadFormState {
    fullName: string | null;
    companyName: string | null;
    phone: string | null;
    workEmail: string | null;
    privacyDisclaimer: boolean;
}

interface FormErrors {
    fullName: string;
    companyName: string;
    phone: string;
    workEmail: string;
    privacyDisclaimer: string
}

const initialFormState: LeadFormState = {
    companyName: null,
    fullName: null,
    phone: null,
    workEmail: null,
    privacyDisclaimer: false
}

function LeadForm(props: LeadFormProps) {
    const [formData, setFormData] = useState<LeadFormState>(initialFormState)
    const [formErrors, setFormErrors] = useState<FormErrors>({
        workEmail: '',
        phone: '',
        companyName: '',
        fullName: '',
        privacyDisclaimer: ''
    })

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const fieldName = event.target.name;
        const fieldValue = event.target.value;
        setFormData({...formData, [fieldName]: fieldValue})
        validateField(fieldName, fieldValue)
    }
    const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const fieldName = event.target.name;
        setFormData({...formData, [fieldName]: !formData.privacyDisclaimer})
    }

    const validateField = (fieldName: string, value: any) => {
        let errorMessage = '';
        let isValid: boolean = true;
        switch (fieldName) {
            case 'fullName':
            case 'companyName':
            case 'phone':
                isValid = value.length > 0
                errorMessage = !isValid ? 'no empty fields' : ''
                break;
            case 'workEmail':
                isValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                errorMessage = value.length > 0 ? !isValid ? 'email is not valid' : '' : 'please insert work email address'
                break;
            default:
                break;
        }
        // console.log('is valid', isValid, 'error message', errorMessage)
        setFormErrors({...formErrors, [fieldName]: errorMessage})
    }

    const downloadFile = (fileUrl: string, extension: string) => {
        const url = window.URL.createObjectURL(new Blob([fileUrl]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `file.${extension}`);
        document.body.appendChild(link);
        link.click();
    }
    const submitForm = async () => {
        //check if there are any form errors
        let formErrorsExist: boolean =
            //form is not dirty or privacy not checked
            (!formData.workEmail?.length || !formData.phone?.length || !formData.fullName?.length || !formData.companyName?.length || !formData.privacyDisclaimer) ||
            //form doesn't pass validation
            !!(formErrors.workEmail.length || formErrors.phone.length || formErrors.fullName.length || formErrors.companyName.length)

        if (!formErrorsExist) {
            const postData = {
                name: formData.fullName,
                company_name: formData.companyName,
                email: formData.workEmail,
                phone: formData.phone
            }
            const res = await api.post('https://u5d6gnw6aj.execute-api.us-east-1.amazonaws.com/api/data', postData);
            try {
                const pdfLink = await api.get(`https://u5d6gnw6aj.execute-api.us-east-1.amazonaws.com/api/file?id=${res.data.id}`)
                if (pdfLink.data.link) {
                    downloadFile(pdfLink.data.link, 'pdf')
                }
            } catch (e) {
                const redirectUrl = 'https://www.joonko.co';
                window.location.href = redirectUrl;
            }
        }
    }
    return (
        <div className="lead-form">
            <div className="lead-form__header">
                <div className="lead-form__header__title">
                    Want to get the full version?
                </div>
                <div className="lead-form__header__subtitle">
                    Fill in the form below:
                </div>
            </div>
            <div className="lead-form__form-inputs">
                <div className="lead-form__form-inputs__input-wrapper">
                    <input name="fullName" className="lead-form__form-inputs__input-wrapper__input" type="text"
                           placeholder="Full Name"
                           onBlur={handleInputChange}
                           onChange={handleInputChange}
                           value={formData.fullName || ''}/>
                    <span className="lead-form__form-inputs__input-wrapper__error">{formErrors.fullName}</span>
                </div>
                <div className="lead-form__form-inputs__input-wrapper">
                    <input name="companyName" className="lead-form__form-inputs__input-wrapper__input" type="text"
                           placeholder="Company Name"
                           onBlur={handleInputChange}
                           onChange={handleInputChange}
                           value={formData.companyName || ''}/>
                    <span className="lead-form__form-inputs__input-wrapper__error">{formErrors.companyName}</span>
                </div>
                <div className="lead-form__form-inputs__input-wrapper">
                    <input name="phone" className="lead-form__form-inputs__input-wrapper__input" type="text"
                           placeholder="Phone"
                           onBlur={handleInputChange}
                           onChange={handleInputChange}
                           value={formData.phone || ''}/>
                    <span className="lead-form__form-inputs__input-wrapper__error">{formErrors.phone}</span>
                </div>
                <div className="lead-form__form-inputs__input-wrapper">
                    <input name="workEmail" className="lead-form__form-inputs__input-wrapper__input" type="text"
                           placeholder="Work Email"
                           onBlur={handleInputChange}
                           onChange={handleInputChange}
                           value={formData.workEmail || ''}/>
                    <span className="lead-form__form-inputs__input-wrapper__error">{formErrors.workEmail}</span>
                </div>
            </div>
            <button className="lead-form__cta-button" onClick={submitForm}>
                Download now {'>>'}
            </button>
            <div className="lead-form__privacy-terms">
                <input
                    className="lead-form__privacy-terms__checkbox"
                    onChange={handleCheckboxChange}
                    type="checkbox"
                    id="privacyDisclaimer"
                    name="privacyDisclaimer"
                    value="privacyDisclaimer"
                />
                <div className="lead-form__privacy-terms__checkbox-label">I agree to the <a rel="noreferrer"
                                                                                            target="_blank"
                                                                                            href="https://joonko.co/privacy-policy">privacy
                    policy</a> including for Joonko to use my contact
                    details to contact me for marketing purposes
                </div>
            </div>
        </div>
    );
}

export default LeadForm;
