import React from 'react';
import './inclusive-workplace-calendar.scss'
import LeadForm from "../../components/lead-form/lead-form";

interface InclusiveWorkplaceCalendarProps {
}

function InclusiveWorkplaceCalendar(props: InclusiveWorkplaceCalendarProps) {
    return (
        <div className="inclusive-workplace-calendar">
            <div className="inclusive-workplace-calendar__content-information">
                <div className="inclusive-workplace-calendar__content-information__header">
                    <div className="inclusive-workplace-calendar__content-information__header__title">
                        2021 Inclusive Workplace Calendar
                    </div>
                    <div className="inclusive-workplace-calendar__content-information__header__subtitle">
                        Whitepaper
                    </div>
                </div>
                <div className="inclusive-workplace-calendar__content-information__content">
                    <div className="inclusive-workplace-calendar__content-information__content__premise">
                        Joonko’s 2021 Inclusive Workplace Calendar aims to help your HR teams with workplace diversity
                        so everyone feels like part of the plan. Amplify meaningful days across the organizational
                        calendar and embrace communal celebrations. This year’s addition also includes tips on
                        maintaining the sense of community while remote with ideas to support workplace inclusion
                        initiaives. We’ve highlighted important (holy)days, including:
                    </div>
                    <div className="inclusive-workplace-calendar__content-information__content__keypoints">
                        <ul>
                            <li>Major religious and ethical days of celebration and observance</li>
                            <li>Social Media tips to showcase your D&I initiatives</li>
                            <li>Activities to help you celebrate all the biggest holidays with inclusion
                                in mind
                            </li>
                        </ul>
                    </div>
                    <div className="inclusive-workplace-calendar__content-information__content__cta">
                        Download our Inclusive Workplace Calendar and celebrate all the best days throughout the year
                        the right way.
                    </div>
                </div>
            </div>
            <div className="inclusive-workplace-calendar__lead-form">
                <LeadForm/>
            </div>
        </div>
    );
}

export default InclusiveWorkplaceCalendar;
